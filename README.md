﻿# Horts

## Nome da Equipe
Lucas Coelho Duarte e Isaque Pereira Lucio

## Turma
353

## Tema
Site de contratação de serviços

## Introdução
Será um site de contratação de serviços, onde o usuário cadastrará os seus serviços para vender entre outros usuários do site, permitindo assim a divulgação do mesmo e beneficio ao vendedor e ao cliente.

## Objetivo Geral
    * Desenvolver um site para ajudar profissionais a vender seus serviços.

## Objetivo Específico
    * Facilitar a divulgação dos serviços dos usuários.
    * Permitir o cadastro de serviços
    * Permitir a contratação de serviços

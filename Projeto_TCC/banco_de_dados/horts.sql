-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 30-Out-2019 às 00:30
-- Versão do servidor: 5.6.15-log
-- PHP Version: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `horts`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `contratado`
--

CREATE TABLE IF NOT EXISTS `contratado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_servico` int(11) NOT NULL,
  `id_profissional` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `local` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Extraindo dados da tabela `contratado`
--

INSERT INTO `contratado` (`id`, `id_servico`, `id_profissional`, `id_cliente`, `local`) VALUES
(16, 58, 1, 2, 'https://www.google.com.br/maps/place/Gaspar+Operária+Nova+Criciúma+Santa+Catarina'),
(17, 61, 1, 2, 'https://www.google.com.br/maps/place/Augusta+Pizzeti+de+Oliveira+Mina+do+Mato+Criciúma+Santa+Catarin');

-- --------------------------------------------------------

--
-- Estrutura da tabela `servicos`
--

CREATE TABLE IF NOT EXISTS `servicos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(20) NOT NULL,
  `valor` varchar(5) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=68 ;

--
-- Extraindo dados da tabela `servicos`
--

INSERT INTO `servicos` (`id`, `tipo`, `valor`, `id_usuario`) VALUES
(66, 'Jardineiro', '2', 1),
(58, 'Jardineiro', '75', 1),
(59, 'Eletricista', '200', 1),
(64, 'Encanador', '11', 2),
(61, 'Encanador', '200', 1),
(62, 'Mecânico', '11', 1),
(63, 'Mecânico', '300', 1),
(65, 'Eletricista', '20', 2),
(57, 'Jardineiro', '150', 2),
(67, 'Mecânico', '12', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(20) NOT NULL,
  `senha` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `telefone` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `senha`, `email`, `telefone`) VALUES
(1, 'Lucas Coelho Duarte', '123456', 'lucascduarte99@gmail.com', '999999999'),
(2, 'Isaque Lucio', '12345678', 'isaque@88gmail.com', '999999999');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

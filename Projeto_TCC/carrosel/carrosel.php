<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        
       
        <title>Celke</title>
        <link href="carrosel/css/bootstrap.css" rel="stylesheet">		
        <link href="carrosel/css/personalizado.css" rel="stylesheet">
    </head>
    <body>
		
		
		<div class="espaco-topo">
			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
				<ol class="carousel-indicators">
	            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
				</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
								<div class="item active">
									<img src="carrosel/imagens/carousel/slide01.jpg" alt="Slide 1">
								</div>
								
								<div class="item">
									<img src="carrosel/imagens/carousel/slide02.jpg" alt="Slide 2">
								</div>
							
				</div>

				<!-- Controls -->
				<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
	
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="carrosel/js/bootstrap.min.js"></script>        
    </body>
</html>